package ru.softdesign.myauth.data.managers;

/**
 * Created by Alexey on 29.12.16.
 */

public class DataManager {

    PreferencesManager mPreferencesManager;


    public boolean isAuthUser(String token) {

        if (token!= null && !token.isEmpty() && !token.equals("")){
            return true;
        } else return false;

    }

    public String getAuthToken(){
        return mPreferencesManager.getAuthToken();
    }
}
