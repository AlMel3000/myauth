package ru.softdesign.myauth.ui.activities;

import android.graphics.Typeface;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.softdesign.myauth.BuildConfig;
import ru.softdesign.myauth.R;
import ru.softdesign.myauth.mvp.presenters.AuthPresenter;
import ru.softdesign.myauth.mvp.presenters.IAuthPresenter;
import ru.softdesign.myauth.mvp.views.IAuthView;
import ru.softdesign.myauth.ui.BaseActivity;
import ru.softdesign.myauth.ui.custom_views.AuthPanel;

public class RootActivity extends BaseActivity implements IAuthView, View.OnClickListener{


    String book_font = "fonts/PTBebasNeueBook.ttf";
    String regular_font = "fonts/PTBebasNeueRegular.ttf";

    AuthPresenter mPresenter = AuthPresenter.getInstance();
    @BindView(R.id.login_btn)
    Button mLoginButton;
    @BindView(R.id.show_catalog_btn) Button mShowCatalogButton;
    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.auth_wrapper)
    AuthPanel mAuthPanel;
    @BindView(R.id.app_name_txt)
    TextView mAppNameTextView;


    //region================life cycle====================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        ButterKnife.bind(this);
        mPresenter.takeView(this);
        mPresenter.initView();

        Typeface CFR = Typeface.createFromAsset(getAssets(), regular_font);
        Typeface CFB = Typeface.createFromAsset(getAssets(), book_font);


        mAppNameTextView.setTypeface(CFR);

        mLoginButton.setOnClickListener(this);
        mShowCatalogButton.setOnClickListener(this);
    }

    @Override
    protected void onDestroy(){
        mPresenter.dropView();
        super.onDestroy();

    }

    //endregion


    //region=========IAuthView===================
    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG){
            showMessage(e.getMessage());
            e.printStackTrace();
        } else{
            showMessage("Извините - что-то пошло не так");
            // TODO: 29.12.2016 send error stacktrace to crashlitycs 
        }

    }

    @Override
    public void showLoad() {
        showProgress();

    }

    @Override
    public void hideload() {
        hideProgress();

    }

    @Override
    public IAuthPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void showLoginBtn() {
        mLoginButton.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideLoginButton() {
        mLoginButton.setVisibility(View.GONE);
    }



    @Override
    public AuthPanel getAuthPanel() {
        return mAuthPanel;
    }
//endregion


    @Override
    public void onBackPressed() {
        if (!mAuthPanel.isIdle()){
            mAuthPanel.setCustomState(AuthPanel.IDLE_STATE);
        } else{
        super.onBackPressed();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.login_btn:
                    mPresenter.clickOnLogin();
                break;

            case R.id.show_catalog_btn:
                mPresenter.clickOnShowCatalog();
                break;
        }
    }
}
