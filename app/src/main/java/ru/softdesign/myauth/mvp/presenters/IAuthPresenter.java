package ru.softdesign.myauth.mvp.presenters;

import android.support.annotation.Nullable;

import ru.softdesign.myauth.mvp.views.IAuthView;

/**
 * Created by admin on 29.12.2016.
 */

public interface IAuthPresenter {

    void takeView(IAuthView authView);
    void dropView();
    void initView();


    @Nullable
    IAuthView getView();

    void clickOnLogin();
    void clickOnFb();
    void clickOnVk();
    void clickOnTwitter();
    void clickOnShowCatalog();

    boolean checkUserAuth();

    boolean checkEmailValid(String email);
    boolean checkPasswordValid(String password);
}
