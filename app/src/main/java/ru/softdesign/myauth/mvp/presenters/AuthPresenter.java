package ru.softdesign.myauth.mvp.presenters;

import android.support.annotation.Nullable;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.softdesign.myauth.mvp.models.AuthModel;
import ru.softdesign.myauth.mvp.views.IAuthView;
import ru.softdesign.myauth.ui.custom_views.AuthPanel;

import static android.R.attr.password;

/**
 * Created by admin on 29.12.2016.
 */

public class AuthPresenter implements IAuthPresenter {

    private static AuthPresenter ourPresenter = new AuthPresenter();
    private AuthModel mAuthModel;
    private IAuthView mIAuthView;
    private static final String EMAIL_PATTERN = "^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%" +
            "&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7" +
            "f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" +
            "\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)" +
            "\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x" +
            "0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])$";

    public AuthPresenter() {
        mAuthModel = new AuthModel();
    }

    public static AuthPresenter getInstance(){
        return ourPresenter;
    }

    @Override
    public void takeView(IAuthView authView) {
        mIAuthView = authView;

    }

    @Override
    public void dropView() {
        mIAuthView= null;

    }

    @Override
    public void initView() {

        if (getView()!=null){
        if (checkUserAuth()){
            getView().hideLoginButton();
        } else {
            getView().showLoginBtn();
        }}

    }

    @Nullable
    @Override
    public IAuthView getView() {
        return mIAuthView;
    }

    @Override
    public void clickOnLogin() {
        if (getView()!=null && getView().getAuthPanel()!= null) {
            if (getView().getAuthPanel().isIdle()) {
                getView().getAuthPanel().setCustomState(AuthPanel.LOGIN_STATE);
            } else {
                //// TODO: 29.12.2016 auth user
                if (checkEmailValid(getView().getAuthPanel().getUserEmail()) &&
                        checkPasswordValid(getView().getAuthPanel().getUserPassword())){
                mAuthModel.loginUser(getView().getAuthPanel().getUserEmail(), getView().getAuthPanel().getUserPassword());
                getView().showMessage("request for user auth");
                } else getView().showMessage("Проверьте правильность введенных данных");
            }

        }

    }

    @Override
    public void clickOnFb() {
        if (getView()!=null){
            getView().showMessage("FB");
        }

    }

    @Override
    public void clickOnVk() {
        if (getView()!=null){
            getView().showMessage("VK");
        }

    }

    @Override
    public void clickOnTwitter() {
        if (getView()!=null){
            getView().showMessage("Twitter");
        }

    }

    @Override
    public void clickOnShowCatalog() {

        if (getView()!=null){
            getView().showMessage("Показать каталог");
        }

    }

    @Override
    public boolean checkUserAuth() {

        return mAuthModel.isAuthUser();
    }

    @Override
    public boolean checkEmailValid(String email) {
        if (!email.isEmpty()){
            Pattern p = Pattern.compile(EMAIL_PATTERN);
            Matcher m = p.matcher(email);
            return m.matches();


        } else return false;

    }

    @Override
    public boolean checkPasswordValid(String password) {
        if (!password.isEmpty()){

            Pattern p = Pattern.compile("^\\S{8,}$");
            Matcher m = p.matcher(password);
            return m.matches();
        }else return false;

    }
}
