package ru.softdesign.myauth.mvp.views;

import android.support.annotation.Nullable;

import ru.softdesign.myauth.mvp.presenters.IAuthPresenter;
import ru.softdesign.myauth.ui.custom_views.AuthPanel;

/**
 * Created by admin on 29.12.2016.
 */

public interface IAuthView {

    void showMessage(String message);
    void showError(Throwable e);

    void showLoad();
    void hideload();

    IAuthPresenter getPresenter();

    void showLoginBtn();
    void hideLoginButton();


    //void testShowLoginCard();

    @Nullable
    AuthPanel getAuthPanel();
}
